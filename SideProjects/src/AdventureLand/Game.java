package AdventureLand;

import java.util.List;
import java.util.Scanner;
import Characters.CharacterFactory;
import Characters.PlayerCharacter;
import Situations.Situation;
import Situations.SituationFactory;

public class Game 
{
	public static void main(String[] args) 
	{
		List<Situation> situationList = SituationFactory.getSituations();
		List<Situation> lukesSituationList = SituationFactory.getLukesSituations();
		List<Situation> abbysSituationList = SituationFactory.getAbbysSituations();
		List<Situation> tommysSituationList = SituationFactory.getTommysSituations();
		List<PlayerCharacter> characterList = CharacterFactory.getCharacters();
		Scanner kb = new Scanner(System.in);

		System.out.println("Welcome to Adventure Land! Please choose your character:");
		System.out.println("--------------------------------------------------------\n");
		for (int i = 0; i < characterList.size(); i++)
		{
			System.out.println(characterList.get(i) + "\n");
		}
		String choice = kb.nextLine();
		for (PlayerCharacter p : characterList)
		{
			String fullName = p.getFirstName() + " " + p.getLastName();
			boolean abbysFullName = fullName.equalsIgnoreCase("Abby Agile");
			boolean lukesFullName = fullName.equalsIgnoreCase("Luke Stalwart");
			boolean tommysFullName = fullName.equalsIgnoreCase("Tommy Idle");
			
			if (choice.equalsIgnoreCase(p.getFirstName()) || choice.equalsIgnoreCase(p.getLastName()) || choice.equalsIgnoreCase(fullName))
				{
					System.out.println("You chose: " + fullName + "\n");
					System.out.println(situationList.get(0) + "\n");
					String situationChoice = kb.nextLine();
					boolean cross = situationChoice.equalsIgnoreCase("cross");
					boolean find = situationChoice.equalsIgnoreCase("find");
					boolean stay = situationChoice.equalsIgnoreCase("stay");

					if (abbysFullName && cross)
					{
						System.out.println("Yey, you did it!");
					}
					else if (lukesFullName && cross)
					{
						System.out.println("\n" + lukesSituationList.get(0) + "\n");
						situationChoice = kb.nextLine();
						
						if (situationChoice.equalsIgnoreCase("wrestle"))
						{
							System.out.println("\n" + lukesSituationList.get(1) + "\n");
							situationChoice = kb.nextLine();
						}
						else if (situationChoice.equalsIgnoreCase("swim"))
						{
							System.out.println("\n" + lukesSituationList.get(2) + "\n");
							situationChoice = kb.nextLine();
						}
						else if (situationChoice.equalsIgnoreCase("eaten"))
						{
							System.out.println("\n" + lukesSituationList.get(3) + "\n");
							situationChoice = kb.nextLine();
						}
					}
					else if (lukesFullName && find)
					{
						System.out.println("\n" + lukesSituationList.get(4) + "\n");
						situationChoice = kb.nextLine();
						
						if (situationChoice.equalsIgnoreCase("proceed"))
						{
							System.out.println("\n" + lukesSituationList.get(5) + "\n");
							situationChoice = kb.nextLine();
						}
						else if (situationChoice.equalsIgnoreCase("turn"))
						{
							System.out.println("\n" + lukesSituationList.get(6) + "\n");
							situationChoice = kb.nextLine();
						}
					}
					else if (lukesFullName && stay)
					{
						System.out.println("\n" + lukesSituationList.get(7) + "\n");
						situationChoice = kb.nextLine();
					}
					else if (tommysFullName && cross)
					{
						System.out.println("Yey, you did it!");
					}
					else
					{
						System.out.println("Invalid entry");
					}
				}
		}
		
		kb.close();
	}
}
