package Characters;

import java.util.ArrayList;
import java.util.List;

public class CharacterFactory 
{
	public static List<PlayerCharacter> getCharacters()
	{
		List<PlayerCharacter> characterList = new ArrayList<PlayerCharacter>();
		characterList.add(new PlayerCharacter("Tommy", "Idle", "Great company, good cook", "Snakes, bullets, sharp objects, sharks, spiders, basically anything deadly", "Remember the kid in school who was always picked last for everything? Yep, that’s Tommy alright. \nHe may not have much in terms of strength or knowledge but he does have one good quality, he knows how to have a good time."));
		characterList.add(new PlayerCharacter("Luke", "Stalwart", "Strong, fast and great fighter", "Poison", "What he doesn’t have in knowledge he more than makes up for in strength and speed. Fluent in more than 100 \ndifferent types of combat styles, he really knows how to give the audience the ultimate fight they’ve been looking for. \nFrom a young age he wanted to be a ballerina but all that changed when his dad signed him up for a karate class."));
		characterList.add(new PlayerCharacter("Abby", "Agile", "Smart, inventor", "Snakes", "Abby may be one of the smartest women on earth but don’t let that fool you, between learning different languages \nand inventing new things she also likes to have a great time. Just try not to invite any snakes to the party or you \nmight just become one of her next inventions."));
		
		return characterList;
	}
}
