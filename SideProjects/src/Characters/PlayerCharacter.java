package Characters;

public class PlayerCharacter
{
	private String firstName;
	private String lastName;
	private String abilities;
	private String weaknesses;
	private String commentary;

	public PlayerCharacter(String firstName, String lastName, String abilities, String weaknesses, String commentary) 
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.abilities = abilities;
		this.weaknesses = weaknesses;
		this.commentary = commentary;
	}

	public String toString() 
	{
		return "Name: " + getFirstName() + " " + getLastName() + "\n" + "Abilities: " + getAbilities() + "\n" + "Weaknesses: " + getWeaknesses() + "\n" + "Commentary: " + getCommentary();
	}

	public String getFirstName() 
	{
		return firstName;
	}
	
	public String getLastName()
	{
		return lastName;
	}

	public String getAbilities() 
	{
		return abilities;
	}

	public String getWeaknesses() 
	{
		return weaknesses;
	}

	public String getCommentary() 
	{
		return commentary;
	}
}
