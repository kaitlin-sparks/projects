
public class Double23 
{
	public static void main(String[] args) 
	{
		int[] numbers = {2, 2};
		
		System.out.println(double23(numbers));
	}
	
	public static boolean double23(int[] nums)
	{
		if (nums[0] == 2 && nums[1] == 2)
		{
			return true;
		}
		else if (nums[0] == 3 && nums[1] == 3)
		{
			return true;
		}
		
		return false;
	}
}
