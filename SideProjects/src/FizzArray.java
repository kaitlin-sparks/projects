import java.util.Arrays;

public class FizzArray {

	public static void main(String[] args) 
	{
		System.out.println(Arrays.toString(fizzArray(5, 10)));
	}
	
	public static int[] fizzArray(int start, int end)
	{
		int[] arr = new int[end - start];
		int counter = 0;
		
		for (int i = start; i < end; i++)
		{
			arr[counter] = i;
			counter++;
		}
		
		return arr;
	}
}
