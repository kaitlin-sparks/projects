
public class FrontTimes 
{
	public static void main(String[] args)
	{
		String str = "WhatsUp";
		System.out.println(frontTimes(str, 3));
	}
	
	public static String frontTimes(String s, int n)
	{
		String t = "";
		String str = s.substring(0, 3);
		
		for (int i = 0; i < n; i++)
		{
			t += str;
		}
		
		return t;
	}
}
