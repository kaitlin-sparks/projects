
public class IsStrictlyIncreasing 
{
	public static void main(String[] args) 
	{
		int[] arr = {2, 4, 5, 9};
		System.out.println(isStrictlyIncreasing(arr));
	}
	
	public static boolean isStrictlyIncreasing(int[] num)
	{
		for (int i = 0; i < num.length - 1; i++)
		{
			if (num[i] > num[i + 1])
			{
				return false;
			}
		}
		
		return true;
	}
}
