import java.util.ArrayList;
import java.util.List;

public class No4LetterWords 
{
	public static void main(String[] args)
	{
		String[] arr = {"Hello", "Nope", "Who", "What"};
		
		List<String> words = no4LetterWords(arr);
		System.out.println(words);
	}
	
	public static List<String> no4LetterWords(String[] words)
	{
		List<String> list = new ArrayList<String>();
		
		for (String s : words)
		{
			if (s.length() != 4)
			{
				list.add(s);
			}
		}
		
		return list;
	}
}
