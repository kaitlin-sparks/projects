import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ReverseList 
{
	public static void main(String[] args) 
	{
		List<String> list = new ArrayList<String>();
		list.add("Hello");
		list.add("Bye");
		list.add("What?");
		list.add("Why?");
		
		System.out.println(reverseList(list));
	}
	
	public static List<String> reverseList(List<String> list1)
	{
		List<String> list2 = new ArrayList<String>();
		Stack<String> stack = new Stack<String>();
		
		stack.addAll(list1);
		int stackSize = stack.size();
		
		for (int i = 0; i < stackSize; i++)
		{
			list2.add(stack.pop());
		}
		
		return list2;
	}
}
