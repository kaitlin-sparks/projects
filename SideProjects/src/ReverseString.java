public class ReverseString 
{
	public static void main(String[] args) 
	{
		String s = "Hello";
		System.out.println(reverseString(s));
	}

	public static String reverseString(String str)
	{
		String s = "";
		
		for (int i = str.length() - 1; i >= 0; i--)
		{
			s += str.charAt(i);
		}
		
		 return s;
	}
}
