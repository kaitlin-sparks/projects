package Situations;

public class Situation
{
	private String text;
	
	public Situation(String text) 
	{
		this.text = text;
	}

	public String getText() 
	{
		return text;
	}

	@Override
	public String toString() 
	{
		return text;
	}
}
