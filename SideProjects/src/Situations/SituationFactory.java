package Situations;

import java.util.ArrayList;
import java.util.List;

public class SituationFactory 
{
	public static List<Situation> getSituations()
	{
		List<Situation> situationList = new ArrayList<Situation>();
		situationList.add(new Situation("Walking through the forest you hear the sound of rushing water and as you follow the sound you soon come accross a large river. \nNot knowing the area very well you debate as to whether you should \"cross\" it, \"find\" another way across or \"stay\" where you are \nand hope someone finds you."));
		
		return situationList;
	}
	
	public static List<Situation> getLukesSituations()
	{
		List<Situation> lukesSituationList = new ArrayList<Situation>();
		//Luke choice "cross"
		lukesSituationList.add(new Situation("You decide to cross the river so you do what all great athletes do and jump in the water. The current is strong but you’re even strong. \nHalf-way across the river you spot a floating object which you write off as just being a log until it starts to get closer and closer to you. \nOnce it is only a few feet in front of you you begin to realize that it is actually an alligator and you’re about to be it’s lunch. \nDo you want to \"wrestle\" the alligator, \"swim\" as fast as you can away from it or give in to nature and be \"eaten\" by it?"));
		lukesSituationList.add(new Situation("Your instincts kick into gear and you begin to wrestle the alligator. The beast is strong but is no match for you. It tries to take a bite \nout of you but you manage to hold it’s mouth open. Suddenly you remember from one of your teachers that if you punch an alligator in the nose it runs away \nso you punch it in the nose. Caught off guard by what just happened the alligator swiftly turns around and heads for land. You swim to shore and rest."));
		lukesSituationList.add(new Situation("You decide to swim as fast as you can to the other side to get away from the alligator. The current pulls the alligator closer and closer to you \nbut your strong arms help to pull you to shore before the alligator gets too close. Once on shore you jump on the alligator and knock it out cold. \nLooks like you’re having alligator stew tonight."));			
		lukesSituationList.add(new Situation("Luke Stalwart doesn’t know defeat. Just as the alligator gets close enough to bite you punch it in the nose. The alligator flees in terror and you swim to shore."));
		//Luke choice "find"
		lukesSituationList.add(new Situation("You decide to find another way. As you walk along the shore you notice a long, rickety bridge that crosses over the river. Once getting to the bridge \nyou start to cross when one of the boards you step on breaks in half and falling into the rushing water below. Do you want to \"proceed\" or \"turn\" back?"));
		lukesSituationList.add(new Situation("Taking your time you slowly begin your trek across the bridge. Some of the boards manage to hold long enough for you to step on the next one. \nBefore you know it you’re on the other side but now you’re faced with another problem. On the other side of the bridge is a hungry looking lion."));
		lukesSituationList.add(new Situation("Upon thinking it over you decide to turn back thinking that it’s not worth risking your life over. You then decide that you will follow the shore \na bit longer and see if there is another, more safe way to cross the river. After a few hours of walking you come across an abandoned looking boat \nso you get in and paddle across. After reaching the other side of the river you set up camp and shortly fall asleep but you’re soon awaken by the roar \nof a lion in the bushes nearby."));
		//Luke choice "stay"
		lukesSituationList.add(new Situation("You decide that staying put would be the best course of action and you set up camp. After gathering fire wood and building a fire you go fishing \nfor food only to come across a rather large and hungry looking alligator. In a situation of \"it’s either him or me\" you fight the alligator \nand without much effort become victorious. After making a decent alligator stew and resting you decide that waiting seems pointless so you swim across the river."));
		return lukesSituationList;
	}
	
	public static List<Situation> getAbbysSituations()
	{
		List<Situation> abbysSituationList = new ArrayList<Situation>();
		
		return abbysSituationList;
	}
	
	public static List<Situation> getTommysSituations()
	{
		List<Situation> tommysSituationList = new ArrayList<Situation>();
		
		return tommysSituationList;
	}
}
