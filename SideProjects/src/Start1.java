public class Start1 
{
	public static void main(String[] args) 
	{
		int[] a = {1, 3, 4};
		int[] b = {4, 2, 4};
		int result = start1(a, b);
		
		System.out.println(result);
	}
	
	public static int start1(int[] a, int[] b)
	{
		int counter = 0;
		
		if (a[0] == 1)
		{
			counter += 1;
		}
		
		if (b[0] == 1)
		{
			counter += 1;
		}

		return counter;
	}
}
