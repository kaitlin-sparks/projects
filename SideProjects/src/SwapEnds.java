import java.util.Arrays;

public class SwapEnds 
{
	public static void main(String[] args) 
	{
		int[] arr = {1, 2, 3, 4};
		System.out.println(Arrays.toString(swapEnds(arr)));
	}

	public static int[] swapEnds(int[] nums)
	{
		int[] swap = nums;
		int number = nums[0];
		
		swap[0] = nums[nums.length - 1];
		swap[swap.length - 1] = number;
		
		return swap;
	}
}
