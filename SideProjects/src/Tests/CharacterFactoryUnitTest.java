package Tests;

import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import Characters.CharacterFactory;
import Characters.PlayerCharacter;

public class CharacterFactoryUnitTest 
{
	private List<PlayerCharacter> characters;
	
	@Before
	public void setUp()
	{
		characters = CharacterFactory.getCharacters();
	}
	
	@Test
	public void get_characters_first_name()
	{
		PlayerCharacter character = characters.get(1);
		Assert.assertEquals("Luke", character.getFirstName());
	}
	
	@Test
	public void get_characters_lasst_name()
	{
		PlayerCharacter character = characters.get(0);
		Assert.assertEquals("Idle", character.getLastName());
	}
	
	@Test
	public void get_characters_abilities()
	{
		PlayerCharacter character = characters.get(2);
		Assert.assertEquals("Smart, inventor", character.getAbilities());
	}
	
	@Test
	public void get_characters_weaknesses()
	{
		PlayerCharacter character = characters.get(1);
		Assert.assertEquals("Poison", character.getWeaknesses());
	}
	
	@Test
	public void get_characters_commentary()
	{
		PlayerCharacter character = characters.get(0);
		Assert.assertEquals("Remember the kid in school who was always picked last for everything?", character.getCommentary().substring(0,69));
	}
}
