package Tests;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import Situations.Situation;
import Situations.SituationFactory;
import org.junit.Assert;

public class SituationFactoryUnitTest 
{
	private List<Situation> situations;
	private List<Situation> lukeSituations;
	private List<Situation> abbySituations;
	private List<Situation> tommySituations;
	
	@Before
	public void setUp()
	{
		situations = SituationFactory.getSituations();
		lukeSituations = SituationFactory.getLukesSituations();
		abbySituations = SituationFactory.getAbbysSituations();
		tommySituations = SituationFactory.getTommysSituations();
	}
	
	@Test
	public void get_situations()
	{
		Situation situation = situations.get(0);
		Assert.assertEquals("Walking through the forest you hear the sound of rushing water", situation.getText().substring(0,62));
		Assert.assertTrue(situations.size() >= 1);
	}
	
	@Test
	public void get_lukes_situations()
	{
		Situation situation = lukeSituations.get(3);
		Assert.assertEquals("Luke Stalwart doesn’t know defeat.", situation.getText().substring(0, 34));
		Assert.assertTrue(lukeSituations.size() >= 1);
	}
	
	@Test
	public void get_abbys_situations()
	{
		Situation situation = abbySituations.get(0);
		Assert.assertEquals("", situation.getText());
		Assert.assertTrue(abbySituations.size() >= 1);
	}
	
	@Test
	public void get_tommys_situations()
	{
		Situation situation = tommySituations.get(0);
		Assert.assertEquals("", situation.getText());
		Assert.assertTrue(tommySituations.size() >= 1);
	}
}
