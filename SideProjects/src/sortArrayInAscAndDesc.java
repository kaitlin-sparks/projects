import java.util.Arrays;

public class sortArrayInAscAndDesc 
{
	public static void main(String[] args) 
	{
		int[] arr = {13, 4, 2, 9, 5, 1, 5, 21, 3};
		System.out.println("This is an ascending array: " + Arrays.toString(ascending(arr)));
		System.out.println("This is a descending array: " + Arrays.toString(descending(arr)));
		
		//OR you could do...
		Arrays.sort(arr);
		System.out.println("This is an array sort ascending: " + Arrays.toString(arr));
	}
	
	private static int[] ascending(int[] arr)
	{
		int num = 0;
		for (int i = 0; i< arr.length; i++)
		{
			for (int j = i + 1; j < arr.length; j++)
			{
				if (arr[i] > arr[j])
				{
					num = arr[i];
					arr[i] = arr[j];
					arr[j] = num;
				}
			}
		}
		return arr;
	}
	
	private static int[] descending(int[] arr)
	{
		int num = 0;
		for (int i = 0; i< arr.length; i++)
		{
			for (int j = i + 1; j < arr.length; j++)
			{
				if (arr[i] < arr[j])
				{
					num = arr[i];
					arr[i] = arr[j];
					arr[j] = num;
				}
			}
		}
		return arr;
	}
}
