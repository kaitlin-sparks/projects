package com.techelevator;

public interface Bill {

		String getName();
		double getValue();

}
