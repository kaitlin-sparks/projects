package com.techelevator;

public interface Coin {

		String getName();
		double getValue();
	}
