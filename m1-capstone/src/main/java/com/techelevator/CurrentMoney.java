package com.techelevator;

public class CurrentMoney {
	
	private double currentMoney = 0;

	public void addMoney(double fedMoney) {
		if(fedMoney == 1.00 || fedMoney == 2.00 || fedMoney == 5.00 || fedMoney == 10.00) {
		currentMoney += fedMoney;
		Log.logAmountRecieved(fedMoney, this);
		System.out.printf("%-10s $%.2f\n", "Current Money Provided:", currentMoney);
		}
		else {
			System.out.println("Please insert only 1, 2, 5, or 10 dollar bills.");
		}	
	}
	
	public boolean subtractMoney(double spentMoney) {
		if (spentMoney <= currentMoney) {
			currentMoney -= spentMoney;
			System.out.printf("%-10s $%.2f\n", "Current Money Provided", currentMoney);
			return true;
		}
		else {
			System.out.println("Not enough funds available for this selection. Please insert more money");
			return false;
		}
	}

	public double getCurrentMoney() {
		return currentMoney;
	}

	public void setCurrentMoney(double currentMoney) {
		this.currentMoney = currentMoney;
	}
	
	
	
	
	
	
	
	
	
		
		
	


	
}
