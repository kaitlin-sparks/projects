package com.techelevator;

public class Dime extends Money implements Coin {
	
	public Dime() {
		super("Dime", 0.10);
	}

}

