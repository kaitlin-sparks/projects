package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Inventory
{
	private List<Item> inventory;
	private List<Item> purchased;
	private Scanner kb = new Scanner(System.in);
	private Map<Item, Integer> itemsSold = new HashMap<Item, Integer>();

	public Inventory() 
	{
		inventory = getInventory();
		purchased = new ArrayList<Item>();
	}

	public List<Item> getInventory()
	{
		try 
		{
			File items = new File("/Users/ksparks/Development/pairs/team2-java-blue-week4-pair-exercises/m1-capstone/vendingmachine.csv");
			Scanner s = new Scanner(items);

			List<Item> vending = new ArrayList<Item>();

			while (s.hasNextLine())
			{
				String line = s.nextLine();
				String[] fileItems = line.split("\\|");
				String index = fileItems[0];
				String itemName = fileItems[1];
				double cost = Double.parseDouble(fileItems[2]);
				String itemType = fileItems[3];
				Item i = new Item(index, itemName, cost, itemType);
				vending.add(i);
			}

			return vending;
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("File not found.");
			return new ArrayList<Item>();
		}
	}

	public Item getCustomerPurchase(CurrentMoney cm)
	{
		Item item;
		do
		{
			System.out.println("What item would you like to purchase?");
			String index = kb.nextLine();
			item = selectProduct(index);
		}
		while (item == null);
		
		if (cm.subtractMoney(item.getCost()))
		{
			Log.logPurchase(item, cm);
			addToReport(item);
			item.subtractAmount();
			return item;
		}
		else
		{
			return null;
		}
	}
	
	public void printSalesReport()
	{
		Log.writeToReport(itemsSold);
	}
	
	private void addToReport(Item i)
	{
		if (itemsSold.containsKey(i))
		{
			int newAmount = itemsSold.get(i) + 1;
			itemsSold.put(i, newAmount);
		}
		else
		{
			itemsSold.put(i, 1);
		}
	}

	public Item selectProduct(String itemIndex)
	{
		for (Item i : inventory)
		{
			if (itemIndex.equalsIgnoreCase(i.getIndex()))
			{
				if (i.getAmount() < 1)
				{
					System.out.println("Out of stock.");
					return null;
				}
				else
				{
					return i;
				}
			}
		}

		System.out.println("Item not found.");
		return null;
	}

	public void printInventory()
	{
		System.out.printf("%-2s %-18s %-6s %-8s %s%n", "", "Item name", "Cost", "Type", "Stock");
		System.out.println("__________________________________________");
		for (Item i : inventory)
		{
			System.out.printf("%-2s %-18s $%-5.2f %-8s %-5d%n", i.getIndex(), i.getItemName(), i.getCost(), i.getItemType(), i.getAmount());
		}
	}

	public void getConsumedItems()
	{
		for (Item i : purchased)
		{
			if (i.getItemType().equals("Chip"))
			{
				System.out.println("Crunch Crunch, Yum!");
			}
			else if (i.getItemType().equals("Candy"))
			{
				System.out.println("Munch Munch, Yum!");
			}
			else if (i.getItemType().equals("Drink"))
			{
				System.out.println("Glug Glug, Yum!");
			}
			else
			{
				System.out.println("Chew Chew, Yum!");
			}
		}
		purchased.clear();
	}
}
