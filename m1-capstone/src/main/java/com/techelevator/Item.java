package com.techelevator;

public class Item 
{	
	private String index;
	private String itemName;
	private double cost;
	private String itemType;
	private int amount;
	
	public Item(String index, String itemName, double cost, String itemType) 
	{
		this.index = index;
		this.itemName = itemName;
		this.cost = cost;
		this.itemType = itemType;
		this.amount = 5;
	}
	
	public String getIndex() 
	{
		return index;
	}
	
	public int getAmount() 
	{
		return amount;
	}

	public void subtractAmount() 
	{
		amount--;
	}
	
	public void resetAmount()
	{
		amount = 5;
	}

	public String getItemName() 
	{
		return itemName;
	}
	
	public double getCost() 
	{
		return cost;
	}
	
	public String getItemType() 
	{
		return itemType;
	}

	@Override
	public String toString() 
	{
		return index + ": " + itemName + ": $" + cost + ": " + itemType + ": " + amount;
	}
}
