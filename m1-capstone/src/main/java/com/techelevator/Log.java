package com.techelevator;

import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Log 
{
	private static File file = new File("/Users/ksparks/Development/pairs/team2-java-blue-week4-pair-exercises/m1-capstone/Log.txt");
	private static File salesReport = new File ("/Users/ksparks/Development/pairs/team2-java-blue-week4-pair-exercises/m1-capstone/SalesReport.txt");
	
	private Log()
	{
	}
	
	public static void writeToReport(Map<Item, Integer> itemMap)
	{
		try (FileWriter writer = new FileWriter(salesReport, true))
		{
			double totalCost = 0;
			for (Entry<Item, Integer> e : itemMap.entrySet())
			{
				totalCost += e.getKey().getCost() * e.getValue();
				String str = String.format("%s|%d%n", e.getKey().getItemName(), e.getValue());
				writer.write(str);
			}
			String total = String.format("%n **Total Sales** $%.2f%n", totalCost);
			writer.write(total);
		}
		catch (Exception e)
		{
			System.out.println("Failed to write Sales Report");
		}
	}
	
	private static void writeToFile(String s)
	{
		try (FileWriter writer = new FileWriter(file, true))
		{
			String str = String.format("%s %s%n", getDateTime(), s);
			writer.write(str);
		}
		catch (Exception e)
		{
			System.out.println("Failed to write to log.");
		}
	}
	
	public static void logPurchase(Item i, CurrentMoney cm)
	{
		String str = String.format("%-18s %2s $%-10.2f $%-5.2f", i.getItemName(), i.getIndex(), cm.getCurrentMoney() + i.getCost(), cm.getCurrentMoney());
		writeToFile(str);
	}
	
	public static void logAmountRecieved(double i, CurrentMoney cm)
	{
		String str = String.format("%-18s    $%-10.2f $0","Feed Money", i, cm.getCurrentMoney());
		writeToFile(str);
	}
	
	public static void logChangeGiven(CurrentMoney cm)
	{
		String str = String.format("%-18s    $%-10.2f $0", "Give Change", cm.getCurrentMoney());
		writeToFile(str);
	}
	
	private static String getDateTime() 
	{
	    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	    Date date = new Date();
	    return dateFormat.format(date);
	}
}
