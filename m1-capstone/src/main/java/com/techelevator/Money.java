package com.techelevator;

public class Money implements Coin {
	
	private String name;
	private double value;
	
	public Money(String name, double value) {
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getValue() {
		return this.value;
	}
}