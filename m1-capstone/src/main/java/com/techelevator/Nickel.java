package com.techelevator;

public class Nickel extends Money implements Coin {

	public Nickel() {
		super("Nickel", 0.05);
	}

}
