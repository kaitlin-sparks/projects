package com.techelevator;

public class Quarter extends Money implements Coin {
	public Quarter() {
		super("Quarter", 0.25);
	}
}

