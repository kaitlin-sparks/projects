package com.techelevator;

import java.util.LinkedHashMap;
import java.util.Map;

public class ReturnChange extends CurrentMoney {
	private static final Coin[] coins = new Coin[] {new Quarter(), new Dime(), new Nickel() };
	
	public Map<Coin, Double> getReturnChange(CurrentMoney  currentMoney) {
		
			double changeAmount = currentMoney.getCurrentMoney();

			Map<Coin, Double> changeMap = new LinkedHashMap<Coin, Double>();
			
			for (Coin coin : coins) {
				if (changeAmount <= 0 ) { break; }
				int cnt = (int) (changeAmount / coin.getValue());
				if (cnt > 0) {
					changeAmount = changeAmount % (coin.getValue() * cnt);
					changeMap.put(coin, (double) cnt);
				}	
			}
			System.out.println("Here is your change: ");
			return changeMap;
			
		}


	}

