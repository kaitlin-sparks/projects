package com.techelevator;


import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import com.techelevator.view.Menu;

public class VendingMachineCLI 
{
	private static final String MAIN_MENU_OPTION_GET_REPORT = "Get Sales Report";
	private static final String MAIN_MENU_OPTION_GIVE_CHANGE = "Give Change";
	private static final String MAIN_MENU_OPTION_FEED_MONEY = "Feed Money";
	private static final String MAIN_MENU_OPTION_DISPLAY_ITEMS = "Display Vending Machine Items";
	private static final String MAIN_MENU_OPTION_PURCHASE = "Purchase";
	private static final String[] MAIN_MENU_OPTIONS = {
		MAIN_MENU_OPTION_FEED_MONEY,
		MAIN_MENU_OPTION_DISPLAY_ITEMS,
		MAIN_MENU_OPTION_PURCHASE,
		MAIN_MENU_OPTION_GIVE_CHANGE,
		MAIN_MENU_OPTION_GET_REPORT
	};

	private Menu menu;
	private Inventory inventory = new Inventory();
	private CurrentMoney currentMoney = new CurrentMoney();
	private Scanner userInput = new Scanner(System.in);	
	private Map<Coin, Double> changeMap;
	private ReturnChange returnChange = new ReturnChange();

	public VendingMachineCLI(Menu menu) 
	{
		this.menu = menu;
		changeMap = new HashMap<Coin, Double>();
	}

	public void run() throws FileNotFoundException 
	{
		while(true) 
		{
			String choice = (String)menu.getChoiceFromOptions(MAIN_MENU_OPTIONS);

			if (choice.equals(MAIN_MENU_OPTION_FEED_MONEY)) 
			{
				System.out.println("Please insert money");
				double fedMoney = userInput.nextDouble();
				currentMoney.addMoney(fedMoney);
			}

			else if(choice.equals(MAIN_MENU_OPTION_DISPLAY_ITEMS)) 
			{
				inventory.printInventory();
			}
			else if(choice.equals(MAIN_MENU_OPTION_PURCHASE)) 
			{
				inventory.getCustomerPurchase(currentMoney);
			}
			else if (choice.equals(MAIN_MENU_OPTION_GIVE_CHANGE))
			{
				currentMoney.getCurrentMoney();
				Log.logChangeGiven(currentMoney);
				changeMap = returnChange.getReturnChange(currentMoney);
				for (Coin coin : changeMap.keySet()) {
					System.out.println(changeMap.get(coin) + " " + coin.getName());
				}
				currentMoney.setCurrentMoney(0);
				inventory.getConsumedItems();
			}
			else if (choice.equals(MAIN_MENU_OPTION_GET_REPORT)) 
			{
				inventory.printSalesReport();
			}
		}
	}

	public static void main(String[] args) throws FileNotFoundException 
	{
		Menu menu = new Menu(System.in, System.out);
		VendingMachineCLI cli = new VendingMachineCLI(menu);
		cli.run();
	}
}
