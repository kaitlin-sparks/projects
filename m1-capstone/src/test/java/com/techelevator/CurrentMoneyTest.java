package com.techelevator;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;


public class CurrentMoneyTest {
	
	
	private CurrentMoney currentMoney;
	
	@Before
	public void SetUp() {
	currentMoney = new CurrentMoney();
	}
	
	@Test
	public void verify_starting_dollars() {
		Assert.assertEquals(0.00, currentMoney.getCurrentMoney(), 0);
	}
	
	@Test
	public void verify_feed_first_five_dollars() {
		 currentMoney.addMoney(5.00);
		 Assert.assertEquals(5.00, currentMoney.getCurrentMoney(), 0);
	}
	
	@Test
	public void verify_feed_one_more_dollars() {
		currentMoney.setCurrentMoney(5.00);
		currentMoney.addMoney(1.00);
		 Assert.assertEquals(6.00, currentMoney.getCurrentMoney(), 0);
	}
	
	@Test
	public void verify_spend_two_dollars() {
		currentMoney.setCurrentMoney(5.00);
		currentMoney.subtractMoney(2.00);
		 Assert.assertEquals(3.00, currentMoney.getCurrentMoney(), 0);
	}
	
	@Test
	public void verify_spend_one_dollar_five_cents() {
		currentMoney.setCurrentMoney(5.00);
		currentMoney.subtractMoney(1.05);
		 Assert.assertEquals(3.95, currentMoney.getCurrentMoney(), 0);
	}
	
	@Test
	public void verify_attempt_to_overspend_() {
		currentMoney.setCurrentMoney(2.00);
		currentMoney.subtractMoney(3.00);
		 Assert.assertEquals(2.00, currentMoney.getCurrentMoney(), 0);
	}
	
	@Test
	public void three_dollars_cannot_be_fed() {
		currentMoney.addMoney(3.00);
		 Assert.assertEquals(0.00, currentMoney.getCurrentMoney(), 0);
	}
	
	@Test
	public void it_is_possible_to_have_three_dollars() {
		currentMoney.setCurrentMoney(1.00);
		currentMoney.addMoney(2.00);
		 Assert.assertEquals(3.00, currentMoney.getCurrentMoney(), 0);
	}
	
	@Test
	public void current_money_0_after_change_returned() {
		currentMoney.setCurrentMoney(1.00);
		currentMoney.subtractMoney(1.00);
		 Assert.assertEquals(0.00, currentMoney.getCurrentMoney(), 0);
	}
	
	

}
