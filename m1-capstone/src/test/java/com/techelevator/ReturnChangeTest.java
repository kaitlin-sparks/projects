package com.techelevator;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ReturnChangeTest {

		private ReturnChange returnChange;
		private CurrentMoney currentMoney;
		private Map<Coin, Double> changeMap = new LinkedHashMap<Coin, Double>();
		
		@Before
		public void SetUp() {
		returnChange = new ReturnChange();
		currentMoney = new CurrentMoney();
		}
		
		@Test
		public void make_one_dollar() {
			changeMap = new LinkedHashMap<Coin, Double>();
			currentMoney.setCurrentMoney(1.00);
			changeMap = returnChange.getReturnChange(currentMoney);
			for (Coin key : changeMap.keySet()) {
				Assert.assertEquals("Quarter", key.getName());
				Assert.assertEquals(4d, changeMap.get(key).doubleValue(),0);
				}
		}
			
			@Test
			public void make_forty_cents() {
				changeMap = new LinkedHashMap<Coin, Double>();
				currentMoney.setCurrentMoney(.40);
				changeMap = returnChange.getReturnChange(currentMoney);
				for (Coin key : changeMap.keySet()) {
					if(key.getName() == "Quarter")
					Assert.assertEquals(1, changeMap.get(key).doubleValue(),0);
					if(key.getName() == "Dime")
						Assert.assertEquals(1, changeMap.get(key).doubleValue(),0);	
					if(key.getName() == "Nickel")
						Assert.assertEquals(1, changeMap.get(key).doubleValue(),0);
					}
			}
		
			
			@Test
			public void make_seventyfive_cents() {
				changeMap = new LinkedHashMap<Coin, Double>();
				currentMoney.setCurrentMoney(.75);
				changeMap = returnChange.getReturnChange(currentMoney);
				for (Coin key : changeMap.keySet()) {
					if(key.getName() == "Quarter")
					Assert.assertEquals(3, changeMap.get(key).doubleValue(),0);
					
					}
			}
			
			@Test
			public void fifty_cents_is_not_five_dimes() {
				changeMap = new LinkedHashMap<Coin, Double>();
				currentMoney.setCurrentMoney(.50);
				changeMap = returnChange.getReturnChange(currentMoney);
				for (Coin key : changeMap.keySet()) {
					if(key.getName() == "Dime")
						Assert.fail("this should fail");	
					}
			}
			
			@Test
			public void make_3_dollars_and_fifteen_cents() {
				changeMap = new LinkedHashMap<Coin, Double>();
				currentMoney.setCurrentMoney(3.15);
				changeMap = returnChange.getReturnChange(currentMoney);
				for (Coin key : changeMap.keySet()) {
					if(key.getName() == "Quarter")
					Assert.assertEquals(12, changeMap.get(key).doubleValue(),0);
					if(key.getName() == "Dime")
						Assert.assertEquals(1, changeMap.get(key).doubleValue(),0);	
					if(key.getName() == "Nickel")
						Assert.assertEquals(1, changeMap.get(key).doubleValue(),0);
					}
			}
			
			@Test
			public void make_3_dollars_and_five_cents() {
				changeMap = new LinkedHashMap<Coin, Double>();
				currentMoney.setCurrentMoney(3.15);
				changeMap = returnChange.getReturnChange(currentMoney);
				for (Coin key : changeMap.keySet()) {
					if(key.getName() == "Quarter")
					Assert.assertEquals(12, changeMap.get(key).doubleValue(),0);
					if(key.getName() == "Nickel")
						Assert.assertEquals(1, changeMap.get(key).doubleValue(),0);
					}
			}	
}

