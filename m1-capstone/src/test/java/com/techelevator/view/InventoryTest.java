package com.techelevator.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import com.techelevator.Inventory;
import com.techelevator.Item;

public class InventoryTest {

	private Inventory inventory;

	@Before
	public void setup()
	{
		inventory = new Inventory();
	}

	@Test
	public void test_select_product()
	{
		Item item = inventory.selectProduct("C1");
		Assert.assertEquals("C1", item.getIndex());
		Assert.assertEquals("Cola", item.getItemName());
		Assert.assertEquals(1.25, item.getCost(), 0.001);
		Assert.assertEquals("Drink", item.getItemType());
	}

	@Test
	public void test_select_product_with_invalid_index()
	{
		Item item = inventory.selectProduct("InvalidIndex");
		Assert.assertEquals(null, item);
	}

	@Test
	public void test_select_product_that_is_out_of_stock()
	{
		Item item = inventory.selectProduct("A1");

		Assert.assertEquals("A1", item.getIndex());
		Assert.assertEquals(4, item.getAmount());
		item = inventory.selectProduct("A1");
		Assert.assertEquals(3, item.getAmount());
		item = inventory.selectProduct("A1");
		Assert.assertEquals(2, item.getAmount());
		item = inventory.selectProduct("A1");
		Assert.assertEquals(1, item.getAmount());
		item = inventory.selectProduct("A1");
		Assert.assertEquals(0, item.getAmount());
		item = inventory.selectProduct("A1");
		Assert.assertEquals(null, item);
	}	
}
