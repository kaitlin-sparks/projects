package com.techelevator.view;

import org.junit.Before;
import org.junit.Test;
import com.techelevator.Item;
import org.junit.Assert;

public class ItemTest {
	
	private Item items;
	
	@Before
	public void setup()
	{
		items = new Item("A1", "Oreos", 1.50, "Cookies");
	}
	
	@Test
	public void test_get_index()
	{
		Assert.assertEquals("A1", items.getIndex());
	}
	
	@Test
	public void test_get_item_name()
	{
		Assert.assertEquals("Oreos", items.getItemName());
	}
	
	@Test
	public void test_get_cost()
	{
		Assert.assertEquals(1.50, items.getCost(), 0.001);
	}
	
	@Test
	public void test_get_item_type()
	{
		Assert.assertEquals("Cookies", items.getItemType());
	}
	
	@Test
	public void test_get_amount()
	{
		Assert.assertEquals(5, items.getAmount());
	}
	
	@Test
	public void test_subtract_amount()
	{
		items.subtractAmount();
		Assert.assertEquals(4, items.getAmount());
	}
	
	@Test
	public void test_reset_amount()
	{
		items.subtractAmount();
		items.subtractAmount();
		items.resetAmount();
		Assert.assertEquals(5, items.getAmount());
	}
}
