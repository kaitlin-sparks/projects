package com.techelevator.dao;

public class Forecast {

	private String parkCode;
	private int fiveDayValue;
	private int low;
	private int high;
	private String forecast;

	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

	public int getFiveDayValue() {
		return fiveDayValue;
	}

	public void setFiveDayValue(int fiveDayValue) {
		this.fiveDayValue = fiveDayValue;
	}

	public int getLow() {
		return low;
	}

	public int getLowCelsius() {
		return (low - 32) * 5 / 9;
	}
	
	public void setLow(int low) {
		this.low = low;
	}

	public int getHigh() {
		return high;
	}
	
	public int getHighCelsius() {
		return (high - 32) * 5 / 9;
	}

	public void setHigh(int high) {
		this.high = high;
	}

	public String getForecast() {
		return forecast;
	}

	public void setForecast(String forecast) {
		this.forecast = forecast;
	}

	public String getMessage()
	{
		String forecastMessage= "";

		switch (forecast) {
		case "snow": 
			forecastMessage ="Pack Snowshoes. ";
			break;
		case "rain": 
			forecastMessage ="Pack rain gear and wear waterproof shoes. ";
			break;
		case "thunderstorms": 
			forecastMessage ="Seek shelter and avoid hiking on exposed ridges. ";
			break;
		case "sun": 
			forecastMessage ="Pack sunblock. ";
			break;
		}

		return forecastMessage;		
	}

	//Changed 
	public String getTemperatureMessage()
	{
		String temperatureMessage = "";
		if (high > 75) {
			temperatureMessage += "Bring an extra gallon of water. ";
		} 
		
		if (low < 20) {
			temperatureMessage += "DANGER! FRIGID TEMPERATURES! DON'T GO OUTSIDE! YOU FOOL! ";
		} 
		
		if ((high - low) >= 20 ) {
			temperatureMessage += "Wear breathable layers.";
		}
		
		return temperatureMessage;
	}
}
