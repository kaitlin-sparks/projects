package com.techelevator.dao;

import java.util.List;

public interface ForecastDAO {

	public List<Forecast> displayAllForecasts (String parkCode);
	public List<Forecast> displayForecastById(String parkCode);	
	
}
