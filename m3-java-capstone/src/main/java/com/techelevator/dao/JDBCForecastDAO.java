package com.techelevator.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JDBCForecastDAO implements ForecastDAO{

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JDBCForecastDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Forecast> displayAllForecasts(String parkCode) {
		List <Forecast> forecasts = new ArrayList<Forecast>();
		String findForecasts = "SELECT * FROM weather WHERE parkcode = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(findForecasts, parkCode);

		while(result.next()) {
			Forecast forcast = mapRowToForecast(result);
			forecasts.add(forcast);
		}

		return forecasts;
	}

	@Override
	public List<Forecast> displayForecastById(String parkCode) {
		List <Forecast> forecasts = new ArrayList<Forecast>();
		String sqlSelectForecastById = "SELECT * FROM weather WHERE parkcode = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSelectForecastById, parkCode);

		while(result.next()) {
			Forecast fore = mapRowToForecast(result);
			forecasts.add(fore);
		}


		return forecasts;
	}

	private Forecast mapRowToForecast(SqlRowSet result) {
		Forecast forecast = new Forecast();
		forecast.setParkCode(result.getString("parkcode"));
		forecast.setFiveDayValue(result.getInt("fivedayforecastvalue"));
		forecast.setLow(result.getInt("low"));
		forecast.setHigh(result.getInt("high"));
		forecast.setForecast(result.getString("forecast"));
		return forecast;
	}
}
