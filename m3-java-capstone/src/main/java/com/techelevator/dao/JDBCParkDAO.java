package com.techelevator.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JDBCParkDAO implements ParkDAO {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JDBCParkDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Park> displayAllParks() {
		List<Park> parks = new ArrayList<Park>();
		String sqlSelectAllParks = "SELECT * FROM park ORDER BY parkname";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSelectAllParks);
		
		while(result.next()) {
			parks.add(mapRowToPark(result));
		}
			
		return parks;
	}
	
	@Override
	public Park displayParksById(String parkCode) {
		String sqlSelectParkById = "SELECT * FROM park WHERE parkcode = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSelectParkById, parkCode);
		Park park = new Park();
		
		while(result.next()) {
			park = mapRowToPark(result);
		}
		
		return park;
	}
	
	private Park mapRowToPark(SqlRowSet result) {
		Park park = new Park();
		park.setParkCode(result.getString("parkcode"));
		park.setParkName(result.getString("parkname"));
		park.setState(result.getString("state"));
		park.setAcreage(result.getInt("acreage"));
		park.setElevation(result.getInt("elevationinfeet"));
		park.setMilesOfTrail(result.getFloat("milesoftrail"));
		park.setNumberOfCampSites(result.getInt("numberofcampsites"));
		park.setClimate(result.getString("climate"));
		park.setYearFounded(result.getInt("yearfounded"));
		park.setAnnualVisitorCount(result.getInt("annualvisitorcount"));
		park.setQuote(result.getString("inspirationalquote"));
		park.setQuoteSource(result.getString("inspirationalquotesource"));
		park.setParkDescription(result.getString("parkdescription"));
		park.setEntryFee(result.getDouble("entryfee"));
		park.setNumberOfSpecies(result.getInt("numberofanimalspecies"));
	
		return park;
	}
}
