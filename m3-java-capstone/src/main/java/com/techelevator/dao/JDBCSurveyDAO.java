package com.techelevator.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JDBCSurveyDAO implements SurveyDAO{

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JDBCSurveyDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<TotalSurveyCount> getSurveyCountAndName() {
		List<TotalSurveyCount> totalSurveys = new ArrayList<TotalSurveyCount>();
		String sqlSelectAllSurveys = "SELECT COUNT(s.parkcode) AS c, p.parkname, s.parkcode FROM survey_result AS s JOIN park AS p ON p.parkcode = s.parkcode "
				                   + "GROUP BY s.parkcode, p.parkname ORDER BY c DESC";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSelectAllSurveys);
		
		while(result.next()) {
			totalSurveys.add(mapRowtoTotalSurveyCount(result));
		}
			
		return totalSurveys;
	}
	
	@Override
	public void saveSurvey(Survey newSurvey) {
		String sqlInsertSurvey = "INSERT INTO survey_result(surveyid, parkcode, emailaddress, state, activitylevel)" +
								 "VALUES(?,?,?,?,?)";
		newSurvey.setSurveyId(getNextId());
		jdbcTemplate.update(sqlInsertSurvey, newSurvey.getSurveyId(), newSurvey.getParkCode(), newSurvey.getEmail(), newSurvey.getState(), newSurvey.getActivityLevel());
	}

	private TotalSurveyCount mapRowtoTotalSurveyCount(SqlRowSet result) {
		TotalSurveyCount totalSurveyCount = new TotalSurveyCount ();
		totalSurveyCount.setCount(result.getInt("c"));
		totalSurveyCount.setParkName(result.getString("parkname"));
		totalSurveyCount.setParkCode(result.getString("parkcode"));
		return totalSurveyCount;
	}
	
	private long getNextId()
	{
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_surveyid')");
		 if(nextIdResult.next())
		 {
			 return nextIdResult.getLong(1);
		 }
		 else
		 {
			 throw new RuntimeException("Something went wrong while getting an id for the new survey");
		 }
	}
}
