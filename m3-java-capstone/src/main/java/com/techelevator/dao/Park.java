package com.techelevator.dao;

public class Park {

	private String parkCode;
	private String state;
	private String parkName;
	private int acreage;
	private int elevation;
	private float milesOfTrail;
	private int numberOfCampSites;
	private String climate;
	private int yearFounded;
	private int annualVisitorCount;
	private String quote;
	private String quoteSource;
	private String parkDescription;
	private double entryFee;
	private int numberOfSpecies;
	
	public String getParkCode() {
		return parkCode;
			
	}
	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getParkName() {
		return parkName;
	}
	
	public void setParkName(String parkName) {
		this.parkName = parkName;
	}
	
	public int getAcreage() {
		return acreage;
	}
	
	public void setAcreage(int acreage) {
		this.acreage = acreage;
	}
	
	public int getElevation() {
		return elevation;
	}
	
	public void setElevation(int elevation) {
		this.elevation = elevation;
	}
	
	public float getMilesOfTrail() {
		return milesOfTrail;
	}
	
	public void setMilesOfTrail(float milesOfTrail) {
		this.milesOfTrail = milesOfTrail;
	}
	
	public int getNumberOfCampSites() {
		return numberOfCampSites;
	}
	
	public void setNumberOfCampSites(int numberOfCampSites) {
		this.numberOfCampSites = numberOfCampSites;
	}
	
	public String getClimate() {
		return climate;
	}
	
	public void setClimate(String climate) {
		this.climate = climate;
	}
	
	public int getYearFounded() {
		return yearFounded;
	}
	
	public void setYearFounded(int yearFounded) {
		this.yearFounded = yearFounded;
	}
	
	public int getAnnualVisitorCount() {
		return annualVisitorCount;
	}
	
	public void setAnnualVisitorCount(int annualVisitorCount) {
		this.annualVisitorCount = annualVisitorCount;
	}
	
	public String getQuote() {
		return quote;
	}
	
	public void setQuote(String quote) {
		this.quote = quote;
	}
	
	public String getQuoteSource() {
		return quoteSource;
	}
	
	public void setQuoteSource(String quoteSource) {
		this.quoteSource = quoteSource;
	}
	
	public String getParkDescription() {
		return parkDescription;
	}
	
	public void setParkDescription(String parkDescription) {
		this.parkDescription = parkDescription;
	}
	
	public double getEntryFee() {
		return entryFee;
	}
	
	public void setEntryFee(double entryFee) {
		this.entryFee = entryFee;
	}
	
	public int getNumberOfSpecies() {
		return numberOfSpecies;
	}
	
	public void setNumberOfSpecies(int numberOfSpecies) {
		this.numberOfSpecies = numberOfSpecies;
	}
}
