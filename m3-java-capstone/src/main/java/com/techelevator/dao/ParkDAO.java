package com.techelevator.dao;

import java.util.List;

public interface ParkDAO {
	
	public List<Park> displayAllParks();
	public Park displayParksById(String parkCode);
}
