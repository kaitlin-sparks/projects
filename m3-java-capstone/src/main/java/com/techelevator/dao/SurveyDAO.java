package com.techelevator.dao;

import java.util.List;

public interface SurveyDAO {
	
	public List<TotalSurveyCount> getSurveyCountAndName(); 
	public void saveSurvey(Survey newSurvey);

}
