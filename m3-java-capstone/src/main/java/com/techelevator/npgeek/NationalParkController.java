package com.techelevator.npgeek;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.techelevator.dao.ForecastDAO;
import com.techelevator.dao.Park;
import com.techelevator.dao.ParkDAO;
import com.techelevator.dao.Survey;
import com.techelevator.dao.SurveyDAO;

@Controller
@SessionAttributes("weather")
public class NationalParkController {

	@Autowired
	private ParkDAO parkDao;
	
	@Autowired
	private ForecastDAO forecastDao;
	
	@Autowired
	private SurveyDAO surveyDao;

	@RequestMapping(path="/", method=RequestMethod.GET)
	public String displayHomePage(HttpServletRequest request, ModelMap map) {
		String selectedDegreeType = request.getParameter("degreeType");

		map.addAttribute("parks", parkDao.displayAllParks());
		return "homePage";
	}
	
	@RequestMapping(path="/detailPage", method=RequestMethod.GET)
	public String displayDetailPage(HttpServletRequest request, ModelMap map) {
				
		String selectedPark = request.getParameter("parkCode");
		Park park = parkDao.displayParksById(selectedPark);
		request.setAttribute("park", park);
		
		map.addAttribute("forecast", forecastDao.displayForecastById(selectedPark));

		String selectedDegreeType = request.getParameter("degreeType");
		
		//Changed this
		if (selectedDegreeType == null || selectedDegreeType == "") {
			map.addAttribute("weather", "F");
		}
		else {
			String currentScale = (String) map.get("weather");
			if (!currentScale.equals(selectedDegreeType)) {
				map.addAttribute("weather", selectedDegreeType);
			}
		}
		
		return "detailPage";
	}
	
	@RequestMapping(path="/surveyPage", method=RequestMethod.GET)
	public String displaySurveyPage(Model model) {

		if(!model.containsAttribute("surveyPage")) {
			model.addAttribute("surveyPage", new Survey());
		}
		
		return "surveyPage";
	}
	
	@RequestMapping(path="/submit", method=RequestMethod.POST)
	public String displaySurveyPage(ModelMap map, @Valid @ModelAttribute("surveyPage") Survey survey, BindingResult result, RedirectAttributes attr) {
		
		if(result.hasErrors()) {
			return "surveyPage";
		}
		
		surveyDao.saveSurvey(survey);
		
		return "redirect:/favoriteParks";
	}
	
	@RequestMapping(path="/favoriteParks", method=RequestMethod.GET)
	public String displayFavoriteParksPage(ModelMap map) {
		
		map.addAttribute("surveys", surveyDao.getSurveyCountAndName());
		
		return "favoriteParks";
	}
}
