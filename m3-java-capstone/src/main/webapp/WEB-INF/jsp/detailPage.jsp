<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="pageTitle" value="detailPage" />
<%@include file="header.jsp"%>

<section class="detail-page">
	<div class="detail-park">
		<div class="detail-thumb"><img src="img/parks/${park.parkCode.toLowerCase()}.jpg" /></div>
	<div class="park-name">
	<c:out value="${park.parkName} (${park.parkCode})" />
	</div>

	<section class="detail-info">
	<div class="detail-description">
		<c:out value="${park.parkDescription}" />
	</div>
	<table>
		<tr>
			<td><b>State: </td>
			<td><c:out value="${park.state}" /></td>
		</tr>
		<tr>
			<td><b>Acreage: </td>
			<td><c:out value="${park.acreage}" /></td>
		</tr>
		<tr>
			<td><b>Elevation: </td>
			<td><c:out value="${park.elevation}" /></td>
		</tr>
		<tr>
			<td><b>Miles of Trails: </td>
			<td><c:out value="${park.milesOfTrail}" /></td>
		</tr>
		<tr>
			<td><b>Number of Campsites: </td>
			<td><c:out value="${park.numberOfCampSites}" /></td>
		</tr>
		<tr>
			<td><b>Climate: </td>
			<td><c:out value="${park.climate}" /></td>
		</tr>
		<tr>
			<td><b>Year Founded: </td>
			<td><c:out value="${park.yearFounded}" /></td>
		</tr>
		<tr>
			<td><b>Annual Visitor Count: </td>
			<td><c:out value="${park.annualVisitorCount}" /></td>
		</tr>
		<tr>
			<td><b>Quote: </td>
			<td><c:out value="${park.quote}" /></td>
		</tr>
		<tr>
			<td><b>Quote Source: </td>
			<td><c:out value="${park.quoteSource}" /></td>
		</tr>
		
		<tr>
			<td><b>Entry Fee: </td>
			<td><fmt:formatNumber value = "${park.entryFee}" type = "currency"/></td>
		</tr>
		<tr>
			<td><b>Number of Animal Species: </td>
			<td><c:out value="${park.numberOfSpecies}" /></td>
		</tr>
	
	</table>
	</section>
</div> <!-- for class "detail-park" -->
	
<section class="forecast">
	<c:url value="/detailPage" var="detailPageURL">
		<c:param name="parkCode">${park.parkCode}</c:param>
		<c:param name="degreeType">${weather}</c:param>
	</c:url>
	<form action="${detailPageURL}" method="GET">
		<label for="degreeType">Choose Temperature Type</label> <select
			name="degreeType" id="degreeType">
			<option value="F">Fahrenheit</option>
			<option value="C">Celsius</option>
		</select> 
		<input type="hidden" name="parkCode" value="${park.parkCode}">
		<input type="hidden" value="${weather}" />
		<input type="submit" value="Submit" />
	</form>
	
	<c:forEach var="f" items="${forecast}">
		<c:choose>
			<c:when test="${f.fiveDayValue == '1'}">
				<div class="day-one">
				<div class="today"><c:out value="Today" /></div>
				<img src="img/weather/${f.forecast.toLowerCase()}.png" />
			
				<c:choose>
					<c:when test="${weather == 'C'}">
						<div class="forecast1"><c:out value="High ${f.highCelsius} C" />
						<c:out value="Low ${f.lowCelsius} C" /></div>
					</c:when>
					<c:otherwise>
						<div class="forecast1"><c:out value="High ${f.high} F" />
						<c:out value="Low ${f.low} F" /></div>
					</c:otherwise>
				</c:choose>
				
				<div class="message"><c:out value="${f.message}" /></div>
				<div class="temp-message"><c:out value="${f.temperatureMessage}" /></div>
			</div>
			</c:when>
		
			<c:otherwise>
				<div class="other-days">
				<img src="img/weather/${f.forecast.toLowerCase()}.png" />
				<c:choose>
					<c:when test="${weather == 'C'}">
						<div class="high"><c:out value="High ${f.highCelsius} C" /></div>
						<div class="low"><c:out value="Low ${f.lowCelsius} C" /></div>
					</c:when>
					<c:otherwise>
						<div class="high"><c:out value="High ${f.high} F" /></div>
						<div class="low"><c:out value="Low ${f.low} F" /></div>
				
					</c:otherwise>
				</c:choose>
					</div>	
			</c:otherwise>	
		</c:choose>
	</c:forEach>
	</section> <!--  for forecast class-->
</section>
<%@include file="footer.jsp"%>



