<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="favoriteParks"/>
<%@include file="header.jsp" %>

<div class="favorite-wrapper">
<h2>Favorite Parks </h2>
	<p>Below is a list of the favorite National Parks based on surveys completed.

	 <c:forEach var="survey" items="${surveys}">
	 	<div class="favorite-thumb"><img src="img/parks/${survey.parkCode.toLowerCase()}.jpg" /></div>
		<div class="favorite-content">
			<div class="favorite-park"><c:out value="${survey.parkName}" /></div>
			<div class="favorite-count"><c:out value="${survey.count}" /></div>
		</div>
	</c:forEach>
</div>





<%@include file="footer.jsp" %>