<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:url value="/css/site.css" var="cssHref" />
	<link rel="stylesheet" href="${cssHref}">


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="${pageTitle}"></c:out></title>

</head>
<body>
	<header>

<c:url value="/img/logo.png" var="logoSrc" />
<img class="header-image" src="${logoSrc}" />
	</header>
	<nav>
		<ul class="header-container">
			<c:url value="/" var="homePageHref" />
			<c:url value="/surveyPage" var="surveyPageHref" />
			<li><a class="url" href="${homePageHref}">Home Page</a></li>
			<li><a class="url" href="${surveyPageHref}">Survey Page</a></li> 
		</ul>
		
	</nav> 
	
</body>