<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="homePage"/>
<%@include file="header.jsp" %>

<h2>Home Page</h2>

<div class="home-page-park">
	<c:forEach var="park" items="${parks}">
	<c:url value="/detailPage" var="detailPageURL" >
		<c:param name="parkCode">${park.parkCode}</c:param>
		<!-- added this -->
		<c:param name="degreeType">${weather}</c:param>
	</c:url>

	<div class="park-thumb">
		<a href="${detailPageURL}">
			<img src="img/parks/${park.parkCode.toLowerCase()}.jpg" class="img"/>
		</a>
	</div>
	<div class="park-content">
		<h3 class="park-name">
			<c:out value="${park.parkName}" />
		</h3>
		<h4 class="park-state">
			<c:out value="${park.state}" />
		</h4>

		<div class="park-description">
			<c:out value="${park.parkDescription}" />
		</div>
	</div>

	</c:forEach>
</div>

<%@include file="footer.jsp" %>