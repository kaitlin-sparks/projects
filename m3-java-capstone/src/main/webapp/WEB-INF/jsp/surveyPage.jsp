<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%-- <c:url value="/css/site.css" var="cssHref" />
	<link rel="stylesheet" href="${cssHref}"> --%>

<c:set var="pageTitle" value="surveyPage" />
<%@include file="header.jsp"%>

<section class="survey-page">
<h2>Survey Page</h2>

<p>Please complete our quick survey for your favorite National Park: 

<h2 cssClass="error">${error}</h2>

<c:url value="/submit" var="surveyPageURL" />

<form:form action="${surveyPageURL}" method="POST" modelAttribute="surveyPage">
	<div class="park-wrapper">
	<div class ="survey-park"><label for="parkCode">Favorite National Park: </label></div>
	<div class="park-entry">
	<form:select path="parkCode">
		<option value=""></option>
		<option value="CVNP">Cuyahoga Valley National Park</option>
		<option value="ENP">Everglades National Park</option>
		<option value="GCNP">Grand Canyon National Park</option>
		<option value="GNP">Glacier National Park</option>
		<option value="GSMNP">Great Smoky Mountains National Park</option>
		<option value="GTNP">Grand Teton National Park</option>
		<option value="MRNP">Mount Rainier National Park</option>
		<option value="RMNP">Rocky Mountain National Park</option>
		<option value="YNP">Yellowstone National Park</option>
		<option value="YNP2">Yosemite National Park</option>
	</form:select>
	<form:errors path="parkCode" cssClass="error" />
	</div>
	</div>

	<div class="email-wrapper">
		<div class="email"><label for="email">Your Email: </label></div>
		<div class="email-input"><form:input path="email" />
		<form:errors path="email" cssClass="error" /></div>
	</div>
<div class="state-wrapper">
	<div class="state"><label for="state">State of Residence: </label></div>
	<div class="state-input">
	<form:select path="state">
		<option value=""></option>
		<option value="AL">Alabama</option>
		<option value="AK">Alaska</option>
		<option value="AZ">Arizona</option>
		<option value="AR">Arkansas</option>
		<option value="CA">California</option>
		<option value="CO">Colorado</option>
		<option value="CT">Connecticut</option>
		<option value="DE">Delaware</option>
		<option value="DC">District Of Columbia</option>
		<option value="FL">Florida</option>
		<option value="GA">Georgia</option>
		<option value="HI">Hawaii</option>
		<option value="ID">Idaho</option>
		<option value="IL">Illinois</option>
		<option value="IN">Indiana</option>
		<option value="IA">Iowa</option>
		<option value="KS">Kansas</option>
		<option value="KY">Kentucky</option>
		<option value="LA">Louisiana</option>
		<option value="ME">Maine</option>
		<option value="MD">Maryland</option>
		<option value="MA">Massachusetts</option>
		<option value="MI">Michigan</option>
		<option value="MN">Minnesota</option>
		<option value="MS">Mississippi</option>
		<option value="MO">Missouri</option>
		<option value="MT">Montana</option>
		<option value="NE">Nebraska</option>
		<option value="NV">Nevada</option>
		<option value="NH">New Hampshire</option>
		<option value="NJ">New Jersey</option>
		<option value="NM">New Mexico</option>
		<option value="NY">New York</option>
		<option value="NC">North Carolina</option>
		<option value="ND">North Dakota</option>
		<option value="OH">Ohio</option>
		<option value="OK">Oklahoma</option>
		<option value="OR">Oregon</option>
		<option value="PA">Pennsylvania</option>
		<option value="RI">Rhode Island</option>
		<option value="SC">South Carolina</option>
		<option value="SD">South Dakota</option>
		<option value="TN">Tennessee</option>
		<option value="TX">Texas</option>
		<option value="UT">Utah</option>
		<option value="VT">Vermont</option>
		<option value="VA">Virginia</option>
		<option value="WA">Washington</option>
		<option value="WV">West Virginia</option>
		<option value="WI">Wisconsin</option>
		<option value="WY">Wyoming</option>
	</form:select>
	<form:errors path="state" cssClass="error" />
		</div>
	</div>
	
	
	<div class="activity-wrapper">
		<div class ="activity">
			<label for="activityLevel">Activity Level: </label>
		</div>
		<div class="activity-input">
			<form:radiobutton path="activityLevel" value="inactive" name="activityLevel" /> Inactive
			<form:radiobutton path="activityLevel" value="sedentary" name="activityLevel" />Sedentary
			<form:radiobutton path="activityLevel" value="active" name="activityLevel" />Active
			<form:radiobutton path="activityLevel" value="extremelyActive" name="activityLevel" />Extremely Active
			<form:errors path="activityLevel" cssClass="error" />
		</div>
	</div>

	<div class="submit"><input type="submit" value="Submit"></div>
</form:form>
</section>
<%@include file="footer.jsp"%>