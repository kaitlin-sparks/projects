package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.techelevator.dao.Forecast;

public class ForecastUnitTest 
{
	private Forecast forecast;
	
	@Before
	public void setUp()
	{
		forecast = new Forecast();
	}
	
	@Test
	public void getParkCode()
	{
		forecast.setParkCode("YNP");
		Assert.assertEquals("YNP", forecast.getParkCode());
	}
	
	@Test
	public void getFiveDayValue()
	{
		forecast.setFiveDayValue(3);
		Assert.assertEquals(3, forecast.getFiveDayValue());
	}
	
	@Test
	public void getLow()
	{
		forecast.setLow(3);
		Assert.assertEquals(3, forecast.getLow());
	}
	
	@Test
	public void getHigh()
	{
		forecast.setHigh(13);
		Assert.assertEquals(13, forecast.getHigh());
	}
	
	@Test
	public void getLowCelsius()
	{
		forecast.setLow(40);
		Assert.assertEquals(4, forecast.getLowCelsius());
	}
	
	@Test
	public void getHighCelsius()
	{
		forecast.setHigh(80);
		Assert.assertEquals(26, forecast.getHighCelsius());
	}
	
	@Test
	public void getForecast()
	{
		forecast.setForecast("snow");
		Assert.assertEquals("snow", forecast.getForecast());
	}
	
	@Test
	public void getMessage()
	{
		forecast.setForecast("snow");
		String message = forecast.getMessage();
		Assert.assertEquals("Pack Snowshoes", message);
		
		forecast.setForecast("rain");
		message = forecast.getMessage();
		Assert.assertEquals("Pack rain gear and wear waterproof shoes", message);
		
		forecast.setForecast("thunderstorms");
		message = forecast.getMessage();
		Assert.assertEquals("Seek shelter and avoid hiking on exposed ridges", message);
		
		forecast.setForecast("sun");
		message = forecast.getMessage();
		Assert.assertEquals("Pack sunblock", message);
	}
	
	@Test
	public void getHighTemperatureMessage()
	{
		forecast.setHigh(80);
		forecast.setLow(79);
		String message = forecast.getTemperatureMessage();
		Assert.assertEquals("Bring an extra gallon of water. ", message);

		forecast.setHigh(11);
		forecast.setLow(10);
		message = forecast.getTemperatureMessage();
		Assert.assertEquals("DANGER! FRIGID TEMPERATURES! DON'T GO OUTSIDE! YOU FOOL! ", message);

		forecast.setHigh(70);
		forecast.setLow(50);
		message = forecast.getTemperatureMessage();
		Assert.assertEquals("Wear breathable layers.", message);

		forecast.setHigh(90);
		forecast.setLow(0);
		message = forecast.getTemperatureMessage();
		Assert.assertEquals("Bring an extra gallon of water. DANGER! FRIGID TEMPERATURES! DON'T GO OUTSIDE! YOU FOOL! Wear breathable layers.", message);
	}
}
