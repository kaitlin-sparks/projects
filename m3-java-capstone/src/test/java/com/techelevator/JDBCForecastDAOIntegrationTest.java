package com.techelevator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.dao.Forecast;
import com.techelevator.dao.JDBCForecastDAO;

public class JDBCForecastDAOIntegrationTest 
{
	private static final String TEST_ID = "YNP";

	private static SingleConnectionDataSource dataSource;
	private JDBCForecastDAO dao;

	@BeforeClass
	public static void setupDataSource()
	{
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/npgeek");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		dataSource.setAutoCommit(false);
	}

	@AfterClass
	public static void closeDataSource()
	{
		dataSource.destroy();
	}

	@Before
	public void setup()
	{
		String sqlInsertForecast = "insert into weather (parkcode, fivedayforecastvalue, high, low, forecast) "
								 + "values (?, 6, 43, 36, 'sunny')";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertForecast, TEST_ID);
		dao = new JDBCForecastDAO(dataSource);
	}

	@After
	public void rollback() throws SQLException
	{
		dataSource.getConnection().rollback();
	}
	
	@Test
	public void test_display_all_forecasts()
	{
		List<Forecast> results = dao.displayAllForecasts(TEST_ID);

		assertNotNull(results);
		assertTrue(results.size() >= 1);
	}
	
	@Test
	public void test_display_forecasts_by_id()
	{
		List<Forecast> results = dao.displayForecastById(TEST_ID);

		assertNotNull(results);
		assertTrue(results.size() >= 1);
	}
}
