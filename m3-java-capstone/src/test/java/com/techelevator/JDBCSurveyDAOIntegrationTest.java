package com.techelevator;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.dao.JDBCSurveyDAO;
import com.techelevator.dao.Survey;
import com.techelevator.dao.TotalSurveyCount;

public class JDBCSurveyDAOIntegrationTest 
{
	private static final long TEST_ID = 1;

	private static SingleConnectionDataSource dataSource;
	private JDBCSurveyDAO dao;

	@BeforeClass
	public static void setupDataSource()
	{
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/npgeek");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		dataSource.setAutoCommit(false);
	}

	@AfterClass
	public static void closeDataSource()
	{
		dataSource.destroy();
	}

	@Before
	public void setup()
	{
		String sqlInsertSurvey = "insert into survey_result (surveyid, parkcode, emailaddress, state, activitylevel) "
								 + "values (?, 'YNP', 'sjhdfg@sjfd.jfd', 'Alabama', 'active')";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertSurvey, TEST_ID);
		dao = new JDBCSurveyDAO(dataSource);
	}

	@After
	public void rollback() throws SQLException
	{
		dataSource.getConnection().rollback();
	}
	
	@Test
	public void test_survey_count_and_name()
	{
		List<TotalSurveyCount> results = dao.getSurveyCountAndName();

		assertNotNull(results);
		assertTrue(results.size() >= 1);
	}
	
	@Test
	public void test_save_survey()
	{
		Survey newSurvey = getSurvey(TEST_ID, "YNP", "sjhdfg@sjfd.jfd", "Alabama", "active");
		
		dao.saveSurvey(newSurvey);
		
		assertNotEquals(null, newSurvey.getSurveyId());
	}
	
	private Survey getSurvey(long id, String parkCode, String email, String state, String activityLevel) 
	{
		Survey surveys = new Survey();
		surveys.setSurveyId(id);
		surveys.setParkCode(parkCode);
		surveys.setEmail(email);
		surveys.setState(state);
		surveys.setActivityLevel(activityLevel);
	
		return surveys;
	}
}
