package com.techelevator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import com.techelevator.dao.JDBCParkDAO;
import com.techelevator.dao.Park;

public class JDBSParkDAOIntegrationTest 
{
	private static final String TEST_ID = "DFG";

	private static SingleConnectionDataSource dataSource;
	private JDBCParkDAO dao;

	@BeforeClass
	public static void setupDataSource()
	{
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/npgeek");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		dataSource.setAutoCommit(false);
	}

	@AfterClass
	public static void closeDataSource()
	{
		dataSource.destroy();
	}

	@Before
	public void setup()
	{
		String sqlInsertPark = "insert into park (parkcode, parkname, state, acreage, elevationinfeet, milesoftrail, numberofcampsites, climate, yearfounded, "
							 + "annualvisitorcount, inspirationalquote, inspirationalquotesource, entryfee, numberofanimalspecies, parkdescription) "
							 + "values (?, 'Parkless', 'Delaware', 71326, 38103762, 2.5, 10, 'Temperate', 1902, 346912, 'Nothing there', 'Lonely Duck', 8.00, 279, 'Not fun to be')";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertPark, TEST_ID);
		dao = new JDBCParkDAO(dataSource);
	}

	@After
	public void rollback() throws SQLException
	{
		dataSource.getConnection().rollback();
	}
	
	@Test
	public void test_display_all_parks()
	{
		List<Park> results = dao.displayAllParks();

		assertNotNull(results);
		assertTrue(results.size() >= 1);
	}
	
	@Test
	public void test_get_Park_by_id()
	{
		Park results = dao.displayParksById(TEST_ID);

		assertNotNull(results);
		assertTrue(results.getParkCode().equals("DFG"));
	}
}
